Name: flex
Version: 2.6.4
Release: 8
Summary: A fast lexical analyzer generator
License: BSD-3-Clause-flex AND GPL-3.0-or-later WITH Autoconf-exception-generic-3.0 AND GPL-3.0-or-later WITH Bison-exception-2.2 AND GPL-3.0-or-later WITH Texinfo-exception AND FSFAP AND FSFUL AND FSFULLR AND FSFULLRWD AND GPL-2.0-or-later AND X11
URL: https://github.com/westes/flex
Source0: https://github.com/westes/flex/releases/download/v%{version}/flex-%{version}.tar.gz

Patch0000: build-AC_USE_SYSTEM_EXTENSIONS-in-configure.ac.patch
Patch0001: filter-memory-leak-free-scanner-postprocessing.patch
Patch0002: scanner-c-i-j-should-preserve-case.patch
Patch0003: filter-Output-correct-line-value-for-current-file.patch
Patch0004: scanner-memory-leak-free-scanner-generator.patch
Patch0005: scanner-Ignore-comment-lines-in-skeleton-files.patch
Patch0006: mkskel.sh-fix-EOL-issue-for-CRLF-systems.patch
Patch0007: scanner-temporarily-protect-against-ccl-overflow-ove.patch
Patch0008: scanner-prevent-overflow-in-add_action.patch
Patch0009: 0001-scanner-fix-default-of-yy_top_state.patch
Patch0010: 0002-scanner-fix-default-of-yy_top_state.patch
Patch0011: 0003-backport-Fix-pattern-documentation-for-case-insensitive-ranges.patch
Patch0012: 0004-backport-fix-typo.patch

BuildRequires: gcc gcc-c++ gettext help2man m4 bison texinfo automake autoconf
Requires: m4

%description
Flex is a tool for generating scanners: programs which recognize lexical
patterns in text.

%package devel
Summary: Libraries for flex scanner generator
Obsoletes: flex-static < 2.5.35-15
Provides: flex-static

%description devel
This package contains the library with default implementations of
`main' and `yywrap' functions that the client binary can choose to use
instead of implementing their own.

%package help
Summary:        Documents for %{name}
Buildarch:      noarch
Requires:	man info
Provides:       flex-doc
Obsoletes:      flex-doc

%description help
Man pages and other related documents for %{name}. 

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf
%configure
%make_build

%install
%make_install
%delete_la
%find_lang %{name}

rm -f %{buildroot}%{_infodir}/dir
rm -f %{buildroot}%{_docdir}/%{name}/{AUTHORS,COPYING,ONEWS,NEWS,README.md}
find %{buildroot} \( -name '*.la' -or -name '*.so' \) -delete
find %{buildroot} \( -name '*.so.2' -or -name '*.so.2.0.0' \) -delete

pushd ${RPM_BUILD_ROOT}
ln -sf flex .%{_bindir}/lex
ln -sf flex .%{_bindir}/flex++
ln -s flex.1 .%{_mandir}/man1/lex.1
ln -s flex.1 .%{_mandir}/man1/flex++.1
ln -s libfl.a .%{_libdir}/libl.a
popd

%check
%make_build check

%files -f %{name}.lang
%license COPYING
%{_bindir}/*
%{_includedir}/FlexLexer.h

%files devel
%license COPYING
%{_libdir}/*.a

%files help
%doc AUTHORS ONEWS NEWS README.md
%{_mandir}/man1/*
%{_infodir}/flex.info*

%changelog
* Sat Aug 10 2024 Funda Wang <fundawang@yeah.net> - 2.6.4-8
- cleanup spec

* Fri Aug 9 2024 guojunding <guojunding@kylinos.cn> - 2.6.4-7
- Fix typo

* Tue Jul 30 2024 guojunding <guojunding@kylinos.cn> - 2.6.4-6
- Fix pattern documentation for case-insensitive ranges

* Tue Jul 12 2022 panxiaohe <panxh.life@foxmail.com> - 2.6.4-5
- enable check test suite
- delete duplicate package files: NEWS,README.md

* Mon Jun 13 2022 panxiaohe <panxh.life@foxmail.com> - 2.6.4-4
- modify license

* Fri May 28 2021 yangzhuangzhuang<yangzhuangzhuang1@huawei.com> - 2.6.4-3
- The autoreconf command is used during compilation.
  Therefore,the automake and autoconf compilation dependencies need to added.

* Sun Jun 28 2020 openEuler liuchengaung<liuchenguang4@huawei.com> - 2.6.4-2
- quality enhancement synchronization github patch

* Mon May 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.6.4-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update to 2.6.4

* Tue Jan 7 2020 chengquan<chengquan3@huawei.com> - 2.6.1-13
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:remove useless patches

* Fri Sep 27 2019 chengquan<chengquan3@huawei.com> - 2.6.1-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add help package

* Mon Aug 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.6.1-11
- Package init

